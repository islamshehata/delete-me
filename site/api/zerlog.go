package handler

import (
	"net/http"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	// UNIX Time is faster and smaller than most timestamps
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	log.Info().Str("github_event_type", "check_suite").Str("github_delivery_id", "REDACTED").Float64("Interval", 833.09).Msg("Received webhook event")
}
