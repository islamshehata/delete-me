
package handler

import (
    "fmt"
    "net/http"
    "time"
    "log"
    "os"
)

func Handler(w http.ResponseWriter, r *http.Request) {
    currentTime := time.Now().Format(time.RFC850)
    log.Println("testing john doe")
    // fmt.Println("fmt.Println to defalt stderr")
    log.SetOutput(os.Stdout)
    // log.Println("log.Println to stdout")
    // fmt.Println("fmt.Println to stdout")
    // fmt.Println("2022-05-10T09:07:17.363Z\t0ebcb157-95c0-465a-8683-10fe6fb1b722\tINFO\tThis doesn't work without a timestamp")
    fmt.Fprintf(w, currentTime)
}
