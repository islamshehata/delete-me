package handler

import (
    "fmt"
    "net/http"
    "time"
    "log"
)

func Handler(w http.ResponseWriter, r *http.Request) {
    currentTime := time.Now().Format(time.RFC850)
    log.Println("testing john doe")
    log.SetOutput(os.Stdout)
    log.Println("printing to stdout")
    fmt.Fprintf(w, currentTime)
}
